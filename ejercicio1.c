#include<stdio.h>
#include<stdlib.h>


struct nodo{
	int info;
	struct nodo *sig;
	};
	
	struct nodo *raiz=NULL;
	
	//crea un nuevo nodo;
	void insertar(int x){
		struct nodo *nuevo;
		nuevo = malloc(sizeof(struct nodo));
		nuevo->info=x;
		if(raiz==NULL){
			raiz=nuevo;
			nuevo->sig=NULL;
			}else{
				nuevo->sig=raiz;
				raiz=nuevo;
				}
		}
		
		
		//imprime los valores de la pila 
		void imprimir(){
			struct nodo *reco=raiz;
			printf("Lista completa.\n");
			while(reco!=NULL){
				printf("%i",reco->info);
				reco=reco->sig;
				printf("\n");	
			}
			}
			
			//remueve un valor de la pila, en este caso el ultimo que se ingresa
			int extraer(){
				if (raiz!=NULL)
				{
					int informacion=raiz->info;
					struct nodo *bor=raiz;
					raiz = raiz->sig;
					free(bor);
					return informacion;
				}else{
					return -1;
					}
				
				}
				
				void liberar(){
					struct nodo *reco= raiz;
					struct nodo *bor;
					while (reco!=NULL)
					{
						bor=reco;
						reco=reco->sig;
						free(bor);
					}
					}
					
					//devuelve la cantidad de nodos dentro de la pila
					int cantidad(){
						struct nodo *reco = raiz;
						int cant =0;
						while (reco!=NULL)
						{
							cant++;
							reco=reco->sig;
						}
						return cant;
						}
						
						
						//Nos permite evaluar si la pila esta vacia
						int vacia(){
							if (raiz==NULL)
							{
								printf("Pila esta vacia\n");
								return 1;

							}
							else
							{
									printf("Pila no esta vacia\n");
								return 0;
							
							}
							
							
							}
					



int main(){
	//ingresamos nodos a la pila
insertar(10);
insertar(40);
insertar(3);
 //imprimimos los valores de la pila
imprimir();
//mostramos la cantidad de nodos en la pila
printf("Cantidad:%i\n",cantidad());
//removemos de la pila un nodo
printf("Extraemos de la pila :%i\n",extraer());
//comprobamos si esta vacia
vacia();
//imprimos de neuvo los valores de la pila
imprimir();
//comprobamos que imprima correctamente la cantidad de nodos despues de remover
printf("Cantidad:%i\n",cantidad());
liberar();
return 0;
	
	}
